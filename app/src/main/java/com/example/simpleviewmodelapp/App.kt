package com.example.simpleviewmodelapp

import android.app.Application
import com.example.simpleviewmodelapp.db.NoteDatabase

class App: Application() {
    val dataBase by lazy { NoteDatabase.getInstance(this) }

    override fun onCreate() {
        super.onCreate()
        SharedPreference.init(this)
    }
}