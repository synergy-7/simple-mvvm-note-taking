package com.example.simpleviewmodelapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.simpleviewmodelapp.App
import com.example.simpleviewmodelapp.db.dao.NoteDao
import com.example.simpleviewmodelapp.model.Note

class NoteViewModel(val noteDao: NoteDao) : ViewModel() {
    private val _notes: MutableLiveData<List<Note>> = MutableLiveData(emptyList())
    val notes get() = _notes

    fun addNote(note: Note) {
        noteDao.insert(note)
    }

    fun getNotes() = noteDao.getAllNote()

    fun deleteNote(idNote: Long) {
        val listNote = mutableListOf<Note>()
        val dataNote = _notes.value
        dataNote?.forEach {
            if (it.noteId != idNote) {
                listNote.add(it)
            }
        }
        _notes.value = listNote
    }
}