package com.example.simpleviewmodelapp.view

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.ViewModelFactoryDsl
import com.example.simpleviewmodelapp.SharedPreference
import com.example.simpleviewmodelapp.ViewModelFactory
import com.example.simpleviewmodelapp.databinding.FragmentSimpleBinding
import com.example.simpleviewmodelapp.model.Note
import com.example.simpleviewmodelapp.viewmodel.NoteViewModel

class SimpleFragment : Fragment() {

    private var _binding: FragmentSimpleBinding? = null
    private val binding get() = _binding!!

    private val viewModel: NoteViewModel by activityViewModels {
        ViewModelFactory.getInstance(requireActivity())
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSimpleBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //untuk read data
        Log.e("SIMPLE_SHARED_PREF", SharedPreference.isLogin.toString())
        binding.btnCount.setOnClickListener {
            val note = Note(noteTitle =  "Contoh Notes", noteContent = "Content Notes")
            viewModel.addNote(note)
            //untuk write data
            SharedPreference.isLogin = true
        }

        viewModel.getNotes().observe(viewLifecycleOwner){
            Log.e("SIMPLENOTES", it.toString())
            //masukin list ke adapter
            //set adapter to recyclerview
            //set layoutmanager recyclerview
        }
    }

}