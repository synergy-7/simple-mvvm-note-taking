package com.example.simpleviewmodelapp.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.simpleviewmodelapp.model.Note

@Dao
interface NoteDao {
    @Insert
    fun insert(note: Note)

    @Query("SELECT * FROM note_table ORDER BY noteId DESC")
    fun getAllNote(): LiveData<List<Note>>
}